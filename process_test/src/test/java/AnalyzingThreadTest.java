import listener.SuiteListener;
import listener.TestListener;
import logger.Log;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import service.DBService;
import sun.awt.windows.ThemeReader;
import thread.AnalyzingThread;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Listeners({TestListener.class, SuiteListener.class})
public class AnalyzingThreadTest {


    @Test(description = "Checks if method setProcessListAndNamesOfProcesses is filling field processesList")
    public void setProcessListAndNamesOfProcessesProcessListIsEmptyTest(){
        Log.debug("Creating new object AnalyzingThread for checking processList");
        AnalyzingThread analyzingThread = new AnalyzingThread();
        Log.debug("Calling method setProcessListAndNamesOfProcesses");
        analyzingThread.setProcessListAndNamesOfProcesses();
        Log.debug("Assertion that Full list of processes is not empty");
        Assert.assertFalse(analyzingThread.getProcessesList().isEmpty(),
                "List of processes is empty");
    }

    @Test(description = "Checks if method setProcessListAndNamesOfProcesses gets needed names (idea64)")
    public void setProcessListAndNamesOfProcessesContainsAppTest(){
        Log.debug("Creating new object AnalyzingThread for checking idea64");
        AnalyzingThread analyzingThread = new AnalyzingThread();
        Log.debug("Calling method setProcessListAndNamesOfProcesses");
        analyzingThread.setProcessListAndNamesOfProcesses();
        Log.debug("Assertion that  list with names of processes contains idea64.exe");
        Assert.assertTrue(analyzingThread.getNamesOfProcesses().contains("idea64.exe"),
                "List with processes don't have idea64 process");
    }

    //test will be failed if history of browser is empty
    @Test(description = "Checks if method setVisitedSitesViaReadDB filling set visitedSites")
    public void setVisitedSitesViaReadDBTest(){
        Log.debug("Creating new object AnalyzingThread for checking visitedSites");
        AnalyzingThread analyzingThread = new AnalyzingThread();
        Log.debug("Opening connection");
        DBService.connect();
        Log.debug("Calling method setVisitedSitesViaReadDB");
        analyzingThread.setVisitedSitesViaReadDB();
        DBService.CloseDB();
        Log.debug("Assertion that Full list of visitedSites is not empty");
        Assert.assertFalse(!analyzingThread.getVisitedSites().isEmpty(),
                "History of browser is empty");
    }

    @DataProvider(name = "Data for comparisonTest")
    public Object[][] dataForComparison() {

        Set<String> setA = new HashSet<String>();
        setA.add("opera.exe");
        setA.add("idea.exe");
        setA.add("paint.exe");

        List<String> listA = new ArrayList<String>();
        listA.add("opera.exe");
        listA.add("idea.exe");
        listA.add("paint.exe");

        Set<String> setB = new  HashSet<String>();
        setB.add("google.exe");
        setB.add("eclipse.exe");
        setB.add("paint.exe");

        Set<String> emptySet = new  HashSet<String>();
        List<String> emptyList = new ArrayList<String>();

        return new Object[][]{
                {setA, listA, 0},
                {setB, listA, 2},
                {setA,emptyList, -1},
                {emptySet, listA, -1}

        };
    }


    @Test(description = "Checks if method compare list<string> and set<string> correct",
            dataProvider = "Data for comparisonTest")
    public void comparisonTest(Set<String> current, List<String> declared, int expectedMismatches){
        AnalyzingThread analyzingThread = new AnalyzingThread();
        analyzingThread.comparison(current, declared);
        int mismatches = analyzingThread.getMismatchCounter();
        Assert.assertEquals(mismatches, expectedMismatches, "Number of mismatches counted wrong");
    }
}
