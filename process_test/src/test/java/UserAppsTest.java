import bo.Constants;
import bo.UserData;
import listener.SuiteListener;
import listener.TestListener;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

@Listeners({TestListener.class, SuiteListener.class})
public class UserAppsTest {

    @DataProvider(name = "List, empty list, process path")
    public Object[][] dataForParseCSV() {
        List<String> emptyList = new ArrayList<String>();
        List<String> appsList = new ArrayList<String>();
        appsList.add("app1.exe");
        appsList.add("app2.exe");
        appsList.add("app3.exe");
        return new Object[][]{
                {appsList, Constants.PROCESSES_FILE_PATH_TEST}
        };
    }

    @Test(dataProvider = "List, empty list, process path")
    public void parseCSVTest(List<String> appsList, String processPath){
        UserData userActions = new UserData();
        Assert.assertEquals(appsList, userActions.parseCSV(processPath), "Wrong parsing in parseCSV method");
    }
}
