import frame.UserVerificationFrame;
import listener.SuiteListener;
import listener.TestListener;
import logger.Log;
import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import javax.swing.*;
import java.awt.*;

@Listeners({TestListener.class, SuiteListener.class})
public class UserVerificationFrameTest {

    @Test(description = "Checks if frame is always on top")
    public void createUserAuthIsAlwaysOnTopTest(){
        Log.debug("Creating new object UserVerificationFrame");
        UserVerificationFrame userVerificationFrame = new UserVerificationFrame();
        Log.debug("Calling method createUserVerification");
        userVerificationFrame.createUserVerification();
        Assert.assertTrue(userVerificationFrame.getFrame().isAlwaysOnTop());
        Log.debug("Disposing frame");
        userVerificationFrame.getFrame().dispose();

    }

    @Test(description = "Checks if default close operation is DO_NOTHING_ON_CLOSE")
    public void createUserAuthDefCloseOperationTest(){
        Log.debug("Creating new object UserVerificationFrame");
        UserVerificationFrame userVerificationFrame = new UserVerificationFrame();
        Log.debug("Calling method createUserVerification");
        userVerificationFrame.createUserVerification();
        Assert.assertEquals(userVerificationFrame.getFrame().getDefaultCloseOperation(), 0, "Default close operation of frame is not 0");
        Log.debug("Disposing frame");
        userVerificationFrame.getFrame().dispose();
    }

    @Test(description = "Checks if frame is resizable")
    public void createUserAuthIsResizableTest(){
        Log.debug("Creating new object UserVerificationFrame");
        UserVerificationFrame userVerificationFrame = new UserVerificationFrame();
        Log.debug("Calling method createUserVerification");
        userVerificationFrame.createUserVerification();
        Assert.assertFalse(userVerificationFrame.getFrame().isResizable());
        Log.debug("Disposing frame");
        userVerificationFrame.getFrame().dispose();
    }
    @Test(description = "Checks if frame is visible")
    public void createUserAuthIsVisibleTest(){
        Log.debug("Creating new object UserVerificationFrame");
        UserVerificationFrame userVerificationFrame = new UserVerificationFrame();
        Log.debug("Calling method createUserVerification");
        userVerificationFrame.createUserVerification();
        Assert.assertTrue(userVerificationFrame.getFrame().isVisible());
        Log.debug("Disposing frame");
        userVerificationFrame.getFrame().dispose();
    }

    @Test(description = "checks if there are ComponentListener")
    public void createUserAuthComponentListenerTest(){
        Log.debug("Creating new object UserVerificationFrame");
        UserVerificationFrame userVerificationFrame = new UserVerificationFrame();
        Log.debug("Calling method createUserVerification");
        userVerificationFrame.createUserVerification();
        Assert.assertTrue(userVerificationFrame.getFrame().getComponentListeners().length>0);
        Log.debug("Disposing frame");
        userVerificationFrame.getFrame().dispose();
    }

    @Test(description = "Checks if there are WindowStateListener")
    public void createUserAuthWindowStateListenerTest(){
        Log.debug("Creating new object UserVerificationFrame");
        UserVerificationFrame userVerificationFrame = new UserVerificationFrame();
        Log.debug("Calling method createUserVerification");
        userVerificationFrame.createUserVerification();
        Assert.assertTrue(userVerificationFrame.getFrame().getWindowStateListeners().length>0);
        Log.debug("Disposing frame");
        userVerificationFrame.getFrame().dispose();
    }

    @Test(description = "Checks if there are WindowListener")
    public void createUserAuthWindowListenerTest(){
        Log.debug("Creating new object UserVerificationFrame");
        UserVerificationFrame userVerificationFrame = new UserVerificationFrame();
        Log.debug("Calling method createUserVerification");
        userVerificationFrame.createUserVerification();
        Assert.assertTrue(userVerificationFrame.getFrame().getWindowListeners().length>0);
        Log.debug("Disposing frame");
        userVerificationFrame.getFrame().dispose();
    }
}
