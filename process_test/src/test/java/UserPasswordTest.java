import bo.Constants;
import bo.UserPassword;
import listener.SuiteListener;
import listener.TestListener;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import java.util.List;

@Listeners({TestListener.class, SuiteListener.class})
public class UserPasswordTest {

    @DataProvider(name = "Filepath, indexes, chars")
    public Object[][] dataForGetPasswordFromFile(){
        char[] password_0 = {'2','3','3','4'};
        char[] password_1 = {'1','2','5','4'};
        char[] password_2 = {'6','5','7','8'};
        char[] password_3 = {'3','3','5','6'};
        char[] password_4 = {'5','0','0','9'};
        return new Object[][]{
                {Constants.PASSWORDS_PATH_TEST, password_0, 0},
                {Constants.PASSWORDS_PATH_TEST, password_1, 1},
                {Constants.PASSWORDS_PATH_TEST, password_2, 2},
                {Constants.PASSWORDS_PATH_TEST, password_3, 3},
                {Constants.PASSWORDS_PATH_TEST, password_4, 4}
        };
    }

    @Test(dataProvider = "Filepath, indexes, chars")
    public void getPasswordsFromFileTest(String path, char[] expectedPassword, int index){
        UserPassword userPassword = new UserPassword(path);
        List<char[]> actualPasswords = userPassword.getPasswords();
        Assert.assertEquals(actualPasswords.get(index), expectedPassword, "Mismatch in password N:" + index);

    }


    @Test(dataProvider = "Filepath, indexes, chars")
    public void getPasswordByIndexTest(String path, char[] expectedPassword, int index){
        UserPassword userPassword = new UserPassword(path);
        char[] actualPassword = userPassword.getPasswordByIndex(index);
        Assert.assertEquals(actualPassword, expectedPassword, "Passwords doesn't match");
    }
}
