package service;

import bo.Constants;
import logger.Log;
import org.sqlite.JDBC;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


public class DBService {
    public static Connection conn;
    public static Statement statmt;
    public static ResultSet resSet;

    // --------CONNECTING TO DATABASE--------
    public static void connect() {
        try {
            Log.debug("Getting connection with " + Constants.DATABASE_URI);
            conn = null;
            conn = DriverManager.getConnection("jdbc:sqlite:" + Constants.DATABASE_URI);
            Log.debug("Database is connected");
        }catch (SQLException e){
            Log.debug("Error in DBService.connect, unable to establish connection");
            e.printStackTrace();
        }
    }

    // --------OUTPUT TABLE--------
    public static Set<String> ReadDB()
    {
        try {
            Log.debug("Starting read data(hosts) from database");
            statmt = conn.createStatement();
            resSet = statmt.executeQuery(Constants.HOSTS_QUERY);
            Log.debug("Query executed");
            Set<String> history = new HashSet<String>();
            while(resSet.next())
            {
                String host = resSet.getString("host");
                Log.debug("Adding host: " + host + " to list");
                history.add(host);
            }
            Log.debug("List of hosts is loaded");
            statmt.executeUpdate(Constants.DELETE_DATA_FROM_MOZ_ORIGINS);
            return history;
        }catch (SQLException e){
            Log.debug("Error while reading DB with history, empty list is returned");
            e.printStackTrace();
            return new HashSet<String>();
        }
    }

    // --------CLOSING--------
    public static void CloseDB()
    {
        try {
            Log.debug("Closing connection...");
            conn.close();
            Log.debug("Closing statement...");
            statmt.close();
            Log.debug("Closing result...");
            resSet.close();
            Log.debug("CONNECTION WITH DATABASE NOW CLOSED");
        }catch (SQLException e){
            Log.debug("Unable to close connection");
            e.printStackTrace();
        }
    }
}
