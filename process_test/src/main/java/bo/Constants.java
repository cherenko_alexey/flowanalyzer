package bo;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Set;

public class Constants {
    public static final String THRESHOLD_TIME = "00:00:05";
    public static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("HH:mm:ss");
    public static final String PASSWORDS_PATH = "src/main/resources/dump/passwords.txt";
    public static final String PASSWORDS_PATH_TEST = "src/test/resources/passwordsTest.txt";
    public static final char[] ADMIN_PASSWORD = {'a','d','m','i','n'};
    public static final String PROCESSES_FILE_PATH = "src/main/resources/dump/processes.csv";
    public static final String HISTORY_FILE_PATH = "src/main/resources/dump/history.csv";
    public static final String PROCESSES_FILE_PATH_TEST = "src/test/resources/appsTest.csv";
    public static final String HOSTS_QUERY = "SELECT host FROM moz_origins;";
    public static final String DATABASE_URI =
            "C:\\Users\\" + new UserData().getName() + "\\AppData\\Roaming\\Mozilla\\Firefox\\Profiles\\atby4jvt.default-release\\places.sqlite";
    public static final String DELETE_DATA_FROM_MOZ_ORIGINS = "DELETE FROM moz_origins";
    public static final String PATH_TO_STATE = "src/main/resources/dump/state.txt";
}
