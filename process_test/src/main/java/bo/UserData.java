package bo;

import logger.Log;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class UserData {
    private String userName;
    private List<String> apps; //list of applications in behavior schema
    private List<String> sites; //list of sites in behavior schema


    public UserData() {
        this.userName = defineUserName(); //method which gets current user name
    }

    public String getName() {
        return userName;
    }

    public List<String> getApps() {
        return apps;
    }

    public List<String> getSites() {
        return sites;
    }

    private String defineUserName(){
        Log.debug("Defining current user name...");
        userName = System.getProperty("user.name");
        return userName;
    }


    public UserData(String processesFile, String historyFile) {
        Log.debug("Constructor of UserData filling lists of processes and history of user");
        this.userName = defineUserName(); //method which gets current user name
        this.apps = parseCSV(processesFile);
        this.sites = parseCSV(historyFile);
        Log.debug("Constructor of UserData is FINISHED");
    }


    public List<String> parseCSV(String filePath){
        try {
            Log.debug("Parsing csv in: " + filePath);
            String string = "";
            Scanner in = new Scanner(new File(filePath));
            while(in.hasNext())
                string += in.nextLine();
            in.close();
            String[] stringApps = string.split(",");
            List<String> listApps = new ArrayList<String>();
            Collections.addAll(listApps,stringApps);
            Log.debug("Data from: " + filePath + " is parsed");
            return listApps;
        }catch (FileNotFoundException e){
            e.printStackTrace();
            Log.debug("FILE NOT FOUND AT METHOD UserData.parseCSV, returned an empty list");
            return new ArrayList<String>(); //return empty list
        }
    }
}
