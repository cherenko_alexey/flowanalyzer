package bo;

import logger.Log;

import javax.imageio.IIOException;
import javax.print.DocFlavor;
import java.io.*;
import java.util.*;

public class UserPassword {
    private List<char[]> passwords;

    public UserPassword(String path) {
        this.passwords = getPasswordsFromFile(path);
    }

    //public for testing
    public List<char[]> getPasswordsFromFile(String path){
        Log.debug("Method UserPassword.getPasswordsFromFile getting data from: " + path);
        try {
            File file = new File(path);
            FileReader fr = new FileReader(file);
            BufferedReader reader = new BufferedReader(fr);
            String line;
            List<char[]> lines = new ArrayList<char[]>();
            while((line = reader.readLine()) != null){
                lines.add(line.toCharArray());
            }
            Log.debug("Method UserPassword.getPasswordsFromFile GOT the data from: " + path);
            return lines;
        }catch (FileNotFoundException e){
            e.printStackTrace();
            Log.debug("FILE NOT FOUND at UserPassword.getPasswordsFromFile, returned empty list");
            return new ArrayList<char[]>();
        }catch (IOException e){
            e.printStackTrace();
            Log.debug("IOException at UserPassword.getPasswordsFromFile, returned empty list");
            return new ArrayList<char[]>();
        }
    }

    public char[] getPasswordByIndex(int index){
        return this.passwords.get(index);
    }

    public List<char[]> getPasswords() {
        return passwords;
    }
}
