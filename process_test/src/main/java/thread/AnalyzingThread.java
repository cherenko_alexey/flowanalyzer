package thread;

import bo.Constants;
import bo.UserData;
import logger.Log;
import org.jutils.jprocesses.JProcesses;
import org.jutils.jprocesses.model.ProcessInfo;
import service.DBService;
import java.text.ParseException;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class AnalyzingThread implements Runnable{

    private Set<String> visitedSites = new HashSet<String>(); //list with current history
    private List<ProcessInfo> processesList; //full list of processes
    private Set<String> namesOfProcesses = new HashSet<String>(); //list will loaded with names of processes
    private int mismatchCounter; // indicates how many wrong progs or sites were activated

    public void run() {
        Log.debug("Method run of AnalyzingThread was started");

        try {
            setProcessListAndNamesOfProcesses();
        }finally {
            Log.debug("AppsAnalizerFailture");
        }

        try {
            setVisitedSitesViaReadDB();
        }finally {
            Log.debug("VisitedSitesFailture");
        }

        UserData userData = new UserData(Constants.PROCESSES_FILE_PATH, Constants.HISTORY_FILE_PATH);
        comparison(getNamesOfProcesses(),userData.getApps());
        comparison(getVisitedSites(),userData.getSites());

        if(getMismatchCounter()>5){
            Log.debug("Too many mismatches");
            Log.debug("Starting User Verification Frame in AnalyzingThread.run");
            //Thread.currentThread().join();
            //стопаем поток
            //запускаем окошко
            //диспозоем окошко(оно диспозаеся само при вводе правильного пароля)
            //запускаем застопаный поток
        }
        Log.debug("Cleaning of processesList");
        this.processesList.clear();
        this.visitedSites.clear();
    }

    public void comparison(Set<String> current, List<String> declared){
        Log.debug("Comparison of lists started");
        if(current.isEmpty() | declared.isEmpty()){
            Log.debug("One of input lists is empty, mismatchCounter = -1");
            this.mismatchCounter = -1;
        }else {
            for(String string : current){
                boolean flag = declared.contains(string);
                if(!flag){
                    Log.debug("OOPS Mismatch...");
                    incrementMismatch();
                }
            }
        }
    }

    public void setProcessListAndNamesOfProcesses(){
        Log.debug("Beginning of method setProcessListAndNamesOfProcesses");
        Log.debug("setting Full process list wia JProcesses.getProcessList()");
        setProcessesList(JProcesses.getProcessList()); //loading full array of processes in var "processesList"
        Log.debug("ProcessList is loaded");

        try {
            Date thresholdTime = Constants.DATE_FORMAT.parse(Constants.THRESHOLD_TIME); //threshold time for avoid background processes
            Log.debug("beginning of cycle of filtration of processes and getting names");
            for(ProcessInfo processInfo : getProcessesList()){

                Date timeOfProcess = Constants.DATE_FORMAT.parse(processInfo.getTime()); //time of process in DATE type
                //if process time is after 00:00:05 and fields COMMAND AND USER aren't empty
                if(timeOfProcess.after(thresholdTime) &&
                        processInfo.getCommand().length()>2 &&
                        processInfo.getUser().length()> 2){
//                    write names of processes in Set NAMES_OF_PROCESSES
                      Log.debug("Process : " + processInfo.getName() + " adding to namesOfProcessList");
                    Log.debug("Process Time: " + processInfo.getTime());
                    Log.debug("Process PID: " + processInfo.getPid());
                    Log.debug("Process Name: " + processInfo.getName());
                    Log.debug("Process Time: " + processInfo.getTime());
                    Log.debug("User: " + processInfo.getUser());
                    Log.debug("Virtual Memory: " + processInfo.getVirtualMemory());
                    Log.debug("Physical Memory: " + processInfo.getPhysicalMemory());
                    Log.debug("CPU usage: " + processInfo.getCpuUsage());
                    Log.debug("Start Time: " + processInfo.getStartTime());
                    Log.debug("Priority: " + processInfo.getPriority());
                    Log.debug("Full command: " + processInfo.getCommand());
                    namesOfProcesses.add(processInfo.getName());
                    Log.debug("End of iteration");
                    Log.debug("------------------");
                }
            }

        }catch (ParseException e){
            e.printStackTrace();
        }
    }

    public List<ProcessInfo> getProcessesList() {
        return processesList;
    }

    public void setProcessesList(List<ProcessInfo> processesList) {
        this.processesList = processesList;
    }

    public Set<String> getNamesOfProcesses() {
        return namesOfProcesses;
    }

    public void setNamesOfProcesses(Set<String> namesOfProcesses) {
        this.namesOfProcesses = namesOfProcesses;
    }

    public int getMismatchCounter() {
        return mismatchCounter;
    }

    public Set<String> getVisitedSites() {
        return visitedSites;
    }

    private void incrementMismatch(){
        Log.debug("Increment mismatch");
        this.mismatchCounter++;
    }

    public void setVisitedSitesViaReadDB() {
        Log.debug("Setting visitedSites via DBService.ReadDB()");
        this.visitedSites = DBService.ReadDB();
    }
}
