package listener;

import logger.Log;
import org.testng.ISuite;
import org.testng.ISuiteListener;

public class SuiteListener implements ISuiteListener {
    public void onStart(ISuite iSuite) {
        Log.debug("Suite started" + iSuite.getName());
    }

    public void onFinish(ISuite iSuite) {
        Log.debug("Suite finished" + iSuite.getName());
    }
}
