package listener;

import logger.Log;
import org.testng.*;

public class TestListener extends TestListenerAdapter {

    @Override
    public void onTestSuccess(ITestResult iTestResult) {
        Log.debug("TEST SUCCESS : " + iTestResult.getName());
    }

    @Override
    public void onTestFailure(ITestResult iTestResult) {
        Log.debug("TEST FAILED : " + iTestResult.getName());
    }

    @Override
    public void onTestSkipped(ITestResult iTestResult) {
        Log.debug("TEST SKIPPED : " + iTestResult.getName());
    }

    @Override
    public void onStart(ITestContext iTestContext) {
        Log.debug("TEST STARTED : " + iTestContext.getName());
    }

    @Override
    public void onFinish(ITestContext iTestContext) {
        Log.debug("TEST FINISHED : " + iTestContext.getName());
    }
}