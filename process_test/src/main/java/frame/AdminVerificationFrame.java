package frame;

import bo.Constants;
import frame.UserVerificationFrame;
import logger.Log;
import org.jnativehook.NativeHookException;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.net.URL;
import java.util.Arrays;

public class AdminVerificationFrame {
    private JFrame frame;

    public void createAdminVerification(){
        Log.debug("Saving current state of system");
        //saving current state of system
        UserVerificationFrame.setState("2");
        Log.debug("Beginning of method createAdminVerification");
        Log.debug("Creating Consumer at Admin Verification");
        try {
            new service.ConsumeEvent();
        }catch (NativeHookException e){
            e.printStackTrace();
        }

        //sets looking  of window
        Log.debug("Creating frame");
        JFrame.setDefaultLookAndFeelDecorated(true);
        frame = new JFrame("Verification");

        //set up frame
        Log.debug("Frame set up");
        frameSetUp(frame);

        //creating password field
        Log.debug("Creating password field");
        final JPasswordField passwordField = new JPasswordField(12);
        passwordField.setEchoChar('*');

        //creating of button
        Log.debug("Creating button 'Submit'");
        JButton button = new JButton("Submit");

        //creating panel
        Log.debug("Creating panel 'Enter Admin password'");
        final JPanel contents = new JPanel(new FlowLayout(FlowLayout.LEFT));
        JLabel label = new JLabel("Enter Admin password ");
        Log.debug("Adding elements to panel");
        contents.add(label);
        contents.add(passwordField);
        contents.add(button);
        //connects JPanel to frame
        Log.debug("Connecting panel to frame");
        frame.setContentPane(contents);

        //creating of listener for checking password
        Log.debug("Creating listener for button");
        ActionListener actionListener = new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                char[] enteredPassword = passwordField.getPassword();
                Log.debug("Button 'Submit' is pressed");
                if(Arrays.equals(enteredPassword, Constants.ADMIN_PASSWORD)){
                    Log.debug("Entered correct password");
                    JOptionPane.showMessageDialog(frame,
                            "Your password is correct.",
                            "Password is correct",
                            JOptionPane.INFORMATION_MESSAGE);
                    frame.dispose();
                    UserVerificationFrame.cleanFile(Constants.PATH_TO_STATE);
                }else {
                    Log.debug("Entered wrong password");
                    JOptionPane.showMessageDialog(frame,
                            "Your password is incorrect.",
                            "Password is incorrect",
                            JOptionPane.ERROR_MESSAGE);
                }
            }
        };
        button.addActionListener(actionListener);
    }

    private void frameSetUp(final JFrame frame){
        //puts window on top of all windows
        Log.debug("SetAlwaysOnTop - True");
        frame.setAlwaysOnTop(true);
        //disable of "close" button
        Log.debug("SET DO_NOTHING_ON_CLOSE");
        frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        //sets location in the center of screen
        Log.debug("SET location of frame in the middle of screen");
        frame.setLocationRelativeTo(null);
        //sets size of window
        Log.debug("SET size of window");
        frame.setPreferredSize(new Dimension(270,120));
        //forbid to change size
        Log.debug("SET Resizable - false");
        frame.setResizable(false);
        Log.debug("Forbidding deactivation of frame");
        forbidDeactivating(frame);
        Log.debug("Forbidding moving of frame");
        forbidMoving(frame);
        Log.debug("Forbidding rolling up of frame");
        forbidRollingUp(frame);
        //sets icon
        Log.debug("SET Icon");
        //TODO
        //new frame.UserVerificationFrame().setIcon(frame);
        frame.pack();
        Log.debug("SetVisible - true");
        frame.setVisible(true);
        Log.debug("Frame set up ended");
    }

    private void forbidRollingUp(final JFrame frame){
        frame.addWindowStateListener(new WindowStateListener() {
            public void windowStateChanged(WindowEvent e) {
                Log.debug("Attempt to roll frame up");
                frame.setState(JFrame.NORMAL);
            }
        });
    }

    private static void forbidMoving(final JFrame frame){
        frame.addComponentListener(new ComponentAdapter() {
            @Override
            public void componentMoved(ComponentEvent e) {
                Log.debug("Attempt to move frame");
                frame.setLocationRelativeTo(null);
            }
        });
    }

    private void forbidDeactivating(final JFrame frame){
        frame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowDeactivated(WindowEvent e) {
                Log.debug("Attempt to switch frame");
                frame.toFront();

                //Log.debug("Full-screen mode is on");
                //if user will try to switch to another window frame become fullscreen
                //TODO
                //setFulScreenMode(frame);
            }
        });
    }

    private void setFulScreenMode(JFrame frame){
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        frame.setSize(screenSize);
    }

    private void setIcon(JFrame frame){
        //sets icon
        Log.debug("Setting icon from : /src/main/resources/frameIcon.png");
        URL iconURL = UserVerificationFrame.class.getResource("/src/main/resources/frameIcon.png");
        ImageIcon icon = new ImageIcon(iconURL);
        frame.setIconImage(icon.getImage());
    }
}
