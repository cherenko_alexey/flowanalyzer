package frame;

import bo.Constants;
import bo.UserPassword;
import logger.Log;
import org.jnativehook.NativeHookException;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.util.Arrays;


public class UserVerificationFrame {

    public UserVerificationFrame() {
        this.userPassword = new UserPassword(Constants.PASSWORDS_PATH);
    }

    public UserPassword getUserPassword() {
        return userPassword;
    }

    private UserPassword userPassword;
    private int index;
    private JFrame frame;
    private JLabel label;
    private int numberOfAttempts = 3;



    public void createUserVerification(){
        Log.debug("Beginning of method createUserVerification");
        Log.debug("Saving current state of system");
        //saving current state of system
        setState("1");

        Log.debug("Genwrating random number for password");
        this.index = generateRandomNumber();

        Log.debug("Creating Consumer at User Verification");
        try {
            new service.ConsumeEvent();
        }catch (NativeHookException e){
            e.printStackTrace();
        }


        //sets looking  of window
        Log.debug("Creating frame");
        JFrame.setDefaultLookAndFeelDecorated(true);
        frame = new JFrame("Verification");


        //set up frame
        Log.debug("Frame set up");
        frameSetUp(frame);

        //creating password field
        Log.debug("Creating password field");
        final JPasswordField passwordField = new JPasswordField(12);
        passwordField.setEchoChar('*');

        //creating of button
        Log.debug("Creating button 'Submit'");
        JButton button = new JButton("Submit");

         //creating panel
        Log.debug("Creating panel 'Enter your own password N:'");
         final JPanel contents = new JPanel(new FlowLayout(FlowLayout.LEFT));
         label = new JLabel("Enter your own password N:" + getIndex());
        Log.debug("Adding elements to panel");
         contents.add(label);
         contents.add(passwordField);
         contents.add(button);
         //connects JPanel to frame
        Log.debug("Connecting panel to frame");
         frame.setContentPane(contents);

        //creating of listener for checking password
        Log.debug("Creating listener for button");
        ActionListener actionListener = new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                char[] enteredPassword = passwordField.getPassword();
                Log.debug("Button 'Submit' is pressed");
                if(Arrays.equals(enteredPassword, userPassword.getPasswordByIndex(index))){
                    Log.debug("Entered correct password");
                    JOptionPane.showMessageDialog(frame,
                            "Your password is correct.",
                            "Password is correct",
                            JOptionPane.INFORMATION_MESSAGE);
                    frame.dispose();
                    cleanFile(Constants.PATH_TO_STATE);
                }else {
                    Log.debug("Entered wrong password");
                    JOptionPane.showMessageDialog(frame,
                            "Your password is incorrect.",
                            "Password is incorrect",
                            JOptionPane.ERROR_MESSAGE);
                    setIndex(generateRandomNumber());
                    label.setText("Enter your own password N:" + getIndex());
                    Log.debug("numberOfAttempts is decreased by 1, now = (" + numberOfAttempts + ")");
                    numberOfAttempts--;
                    if(numberOfAttempts == 0){

                        //redirect to admin verification window
                        Log.debug("Disposing User Verification Frame");
                        frame.dispose();
                        cleanFile(Constants.PATH_TO_STATE);
                        Log.debug("Redirecting to Admin Verification Frame");
                        AdminVerificationFrame adminVerificationFrame = new AdminVerificationFrame();
                        adminVerificationFrame.createAdminVerification();
                    }
                }
            }
        };
        button.addActionListener(actionListener);
    }

    private void frameSetUp(final JFrame frame){
        //puts window on top of all windows
        Log.debug("SetAlwaysOnTop - True");
        frame.setAlwaysOnTop(true);
        //disable of "close" button
        Log.debug("SET DO_NOTHING_ON_CLOSE");
        frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        //sets location in the center of screen
        Log.debug("SET location of frame in the middle of screen");
        frame.setLocationRelativeTo(null);
        //sets size of window
        Log.debug("SET size of window");
        frame.setPreferredSize(new Dimension(270,120));
        //forbid to change size
        Log.debug("SET Resizable - false");
        frame.setResizable(false);
        Log.debug("Forbidding deactivation of frame");
        forbidDeactivating(frame);
        Log.debug("Forbidding moving of frame");
        forbidMoving(frame);
        Log.debug("Forbidding rolling up of frame");
        forbidRollingUp(frame);
        //sets icon
        Log.debug("SET Icon");
        //TODO
        //new frame.UserVerificationFrame().setIcon(frame);
        frame.pack();
        Log.debug("SetVisible - true");
        frame.setVisible(true);
        Log.debug("Frame set up ended");
    }

    private void forbidRollingUp(final JFrame frame){
        Log.debug("ForbiddingRollingUp");
        frame.addWindowStateListener(new WindowStateListener() {
            public void windowStateChanged(WindowEvent e) {
                Log.debug("Attempt to roll frame up");
                frame.setState(JFrame.NORMAL);
            }
        });
    }

    private static void forbidMoving(final JFrame frame){
        Log.debug("ForbiddingMoving");
        frame.addComponentListener(new ComponentAdapter() {
            @Override
            public void componentMoved(ComponentEvent e) {
                Log.debug("Attempt to move frame");
                frame.setLocationRelativeTo(null);
            }
        });
    }

    private void forbidDeactivating(final JFrame frame){
        Log.debug("ForbiddingDeactivating");
        frame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowDeactivated(WindowEvent e) {
                Log.debug("Attempt to switch frame");
                frame.toFront();

                //Log.debug("Full-screen mode is on");
                //if user will try to switch to another window frame become fullscreen
                //TODO
                //setFulScreenMode(frame);
            }
        });
    }

    private void setFulScreenMode(JFrame frame){
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        frame.setSize(screenSize);
    }

    private void setIcon(JFrame frame){
        //sets icon
        Log.debug("Setting icon from : /src/main/resources/frameIcon.png");
        URL iconURL = UserVerificationFrame.class.getResource("/src/main/resources/frameIcon.png");
        ImageIcon icon = new ImageIcon(iconURL);
        frame.setIconImage(icon.getImage());

    }

    private int generateRandomNumber(){
        Log.debug("Generating random number");
        return (int)(Math.random()*5);
    }

    public JFrame getFrame() {
        return frame;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public int getIndex() {
        return index;
    }



    public static void cleanFile(String path){
        try {
            FileWriter fstream1 = new FileWriter(path);// конструктор с одним параметром - для перезаписи
            BufferedWriter out1 = new BufferedWriter(fstream1); //  создаём буферезированный поток
            out1.write(""); // очищаем, перезаписав поверх пустую строку
            out1.close(); // закрываем
        } catch (Exception e){
            e.printStackTrace();
            Log.debug("Error in file cleaning");
        }
    }

    public static void setState(String state){
        try {
            FileWriter writer = new FileWriter(Constants.PATH_TO_STATE);
            writer.write(state);
            writer.close();
        }catch (IOException e){
            e.printStackTrace();
        }
    }
}

