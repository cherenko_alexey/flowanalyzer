package cli;

import com.beust.jcommander.IParameterValidator;
import com.beust.jcommander.ParameterException;

import java.io.File;

public class FileExist implements IParameterValidator {
    public void validate(String name, String value) throws ParameterException {
        File file = new File(value);
        if(!file.exists()){
            throw new ParameterException("FileParameterException : File by path " + value + " is not exist");
        }
    }
}
