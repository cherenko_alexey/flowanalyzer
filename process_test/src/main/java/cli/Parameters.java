package cli;

import com.beust.jcommander.Parameter;

public class Parameters {
    private static Parameters instance;

    @Parameter(names = "--help", help = true, description = "HELP")
    private boolean help = false;

    @Parameter(names = {"--log","-l"}, validateWith = FileExist.class, description = "Path to log config file")
    private String pathToLogConfig;

    @Parameter(names = {"--output","o"}, validateWith = FileExist.class, description = "Path to logs output")
    private String pathToLogOutput;

    @Parameter(names = {"--hist","h"}, validateWith = FileExist.class, description = "Path to users history")
    private String pathToHistory;

    @Parameter(names = {"--process","-p"}, validateWith = FileExist.class, description = "Path to users processes")
    private String pathToProcesses;

    @Parameter(names = {"--password","-passwd"}, validateWith = FileExist.class, description = "Path to users passwords")
    private String pathToPasswors;

    @Parameter(names = {"--icon","-i"}, validateWith = FileExist.class, description = "Path to icon")
    private String pathToIconOfWindow;

    @Parameter(names = {"--mismatches","-m"}, validateWith = PositiveInteger.class, description = "Number of mismatches with declared profile")
    private int numberOfMismatches;

    @Parameter(names = {"--pcount","-po"}, validateWith = PositiveInteger.class, description = "Number of passwords in private user massive")
    private int numberOFPasswords;

    @Parameter(names = {"--friquency","-f"}, validateWith = PositiveInteger.class, description = "Friquency of measuring")
    private int friquencyOfMeasuring;






    public static synchronized Parameters getInstance() {
        if (instance == null) {
            instance = new Parameters();
        }
        return instance;
    }

    public Parameters() {
    }

    public boolean isHelp() {
        return help;
    }

    public String getPathToLogConfig() {
        return pathToLogConfig;
    }

    public String getPathToLogOutput() {
        return pathToLogOutput;
    }

    public String getPathToHistory() {
        return pathToHistory;
    }

    public String getPathToProcesses() {
        return pathToProcesses;
    }

    public String getPathToPasswors() {
        return pathToPasswors;
    }

    public String getPathToIconOfWindow() {
        return pathToIconOfWindow;
    }

    public int getNumberOfMismatches() {
        return numberOfMismatches;
    }

    public int getNumberOFPasswords() {
        return numberOFPasswords;
    }

    public int getFriquencyOfMeasuring() {
        return friquencyOfMeasuring;
    }
}
