package runner;

import bo.Constants;
import com.sun.jna.platform.win32.WinNT;
import frame.AdminVerificationFrame;
import frame.UserVerificationFrame;
import logger.Log;
import org.sqlite.core.DB;
import service.DBService;

import javax.swing.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Runner {
    public static void main(String[] args){

        try{
            String string = "";
            Scanner in = new Scanner(new File(Constants.PATH_TO_STATE));
            while(in.hasNext())
                string += in.nextLine();
            in.close();
            if(string == "1"){
                SwingUtilities.invokeLater(new Runnable() {
                   public void run() {
                     UserVerificationFrame userVerificationFrame = new UserVerificationFrame();
                     userVerificationFrame.createUserVerification();
                   }
                 });
            }
            if (string == "2"){
                SwingUtilities.invokeLater(new Runnable() {
                    public void run() {
                        AdminVerificationFrame adminVerificationFrame = new AdminVerificationFrame();
                        adminVerificationFrame.createAdminVerification();
                    }
                });
            }
        }catch (FileNotFoundException e){
            e.printStackTrace();
        }

//        DBService.connect();
//        parsing input parameters
//        Parameters parameters = new Parameters();
//        JCommander jct = JCommander.newBuilder().addObject(parameters).build();
//        jct.parse(args);
//        if(parameters.isHelp()){
//            jct.usage();
//        }

        //launch of analyzing thread
//        try {
//            ScheduledExecutorService service = Executors.newSingleThreadScheduledExecutor();
//            service.scheduleWithFixedDelay(new thread.AnalyzingThread(), 0,10, TimeUnit.SECONDS);
//        }finally {
//            Log.debug("AnalyzationFlowFailture");
//        }


        //launch of frame
//        SwingUtilities.invokeLater(new Runnable() {
//            public void run() {
//                UserVerificationFrame userVerificationFrame = new UserVerificationFrame();
//                userVerificationFrame.createUserVerification();
//            }
//        });

//            DBService.CloseDB();
    }
}
